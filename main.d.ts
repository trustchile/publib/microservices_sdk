// eslint-disable-next-line @typescript-eslint/no-unused-vars
import * as ts from 'typescript'

declare global {
  namespace NodeJS {
    interface ProcessEnv {
      SERVICE_URI_AUDIT: string
      SERVICE_URI_NOTIFICATIONS: string
      SERVICE_URI_IDENTIFICATION: string
      SERVICE_URI_COMPANY: string
      SERVICE_URI_CAMPAIGNS: string
      SERVICE_URI_CHANNELS: string
      SERVICE_URI_MEDUSA: string
      SERVICE_URI_INDEX: string
      SERVICE_URI_SIGNAL: string
      SERVICE_URI_MESSAGE_PERSISTENCE: string
      SERVICE_URI_BIO_GATEWAY: string
      SERVICE_URI_ATENEA: string
    }
  }
}
