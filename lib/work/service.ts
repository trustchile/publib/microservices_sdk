import StatusCodes from 'http-status-codes'
import { GaxiosError } from 'gaxios'
import { plainToClass } from 'class-transformer'
import { validateOrReject } from 'class-validator'
import { Singleton, HttpClient } from '../common'
import { ST, DTO, DAO } from './index'

/**
 *
 */
@Singleton()
export class Service extends HttpClient {
  /**
   *
   * @param companyId -
   * @param appId -
   */
  public async findFlowsByAppId(companyId: string | number, appId: string | number) {
    try {
      const response = await this.$httpGet<ST.findFlowsByAppIdResponse>(
        `/v1/flow?app_id=${appId}&company_id=${companyId}`
      )

      return response?.data ?? []
    } catch (e) {
      if (e instanceof GaxiosError) {
        const error = e as GaxiosError
        if (error && error.code === String(StatusCodes.NOT_FOUND) && error.response && error.response.data) {
          return []
        } else {
          throw e
        }
      } else {
        throw e
      }
    }
  }

  /**
   *
   * @param companyId -
   * @param flavorId -
   */
  public async findFlowsByFlavorId(companyId: string | number, flavorId: string) {
    try {
      const response = await this.$httpGet<ST.findFlowsByFlavorIdResponse>(
        `/v1/flow?flavor_id=${flavorId}&company_id=${companyId}`
      )

      return response?.data ?? []
    } catch (e) {
      if (e instanceof GaxiosError) {
        const error = e as GaxiosError
        if (error && error.code === String(StatusCodes.NOT_FOUND) && error.response && error.response.data) {
          return []
        } else {
          throw e
        }
      } else {
        throw e
      }
    }
  }

  /**
   *
   * @param flowId -
   */
  public async findFlowById(flowId: string) {
    const response = await this.$httpGet<ST.findFlowByIdResponse>(`/v1/flow/${flowId}`)
    return response?.data ?? null
  }

  /**
   *
   * @param companyId -
   * @param name -
   * @param appId -
   */
  public async createFlow(companyId: string | number, name: string, appId: string | number) {
    const response = await this.$httpPost<ST.createFlowResponse>(`/v1/flow`, {
      name: name,
      app_id: String(appId),
      company_id: String(companyId)
    })
    return response?.data ?? null
  }

  /**
   *
   * @param flowUuid -
   * @param payload -
   */
  public async updateFlow(flowUuid: string, payload: any) {
    const response = await this.$httpPut<ST.updateFlowResponse>(`/v1/flow/${flowUuid}`, payload)
    return response?.data ?? null
  }

  /**
   *
   * @param flowUuid -
   * @param viewUuid -
   */
  public async deleteView(flowUuid: string, viewUuid: string) {
    const response = await this.$httpDelete<any>(`/v1/flow/${flowUuid}/view/${viewUuid}`)
    return response?.data ?? null
  }

  /**
   *
   * @param companyId -
   * @param flowUuid -
   */
  public async findAnswersByFlowId(companyId: string | number, flowUuid: string) {
    try {
      const response = await this.$httpGet<ST.findAnswersByFlowIdResponse>(
        `/v1/flow/${flowUuid}/payload?company_id=${companyId}`
      )
      return response?.data ?? []
    } catch (e) {
      return []
    }
  }

  /**
   *
   * @param flowUuid -
   * @param payloadUuid -
   */
  public async findAnswerById(flowUuid: string, payloadUuid: string) {
    try {
      const response = await this.$httpGet<ST.findAnswerByIdResponse>(`/v1/flow/${flowUuid}/payload/${payloadUuid}`)
      return response?.data ?? []
    } catch (e) {
      return []
    }
  }

  /**
   *
   * @param flowUuid -
   * @param payload -
   */
  public async createView(flowUuid: string, view: DAO.CreateView) {
    const ViewPayload = plainToClass(DTO.CreateView, view)
    await validateOrReject(ViewPayload)
    const response = await this.$httpPost<ST.createViewResponse>(`/v1/flow/${flowUuid}/view`, ViewPayload)
    return response?.data ?? null
  }

  /**
   *
   * @param flowUuid -
   * @param viewUuid -
   * @param payload -
   */
  public async updateView(flowUuid: string, viewUuid: string, view: DAO.UpdateView) {
    const ViewPayload = plainToClass(DTO.UpdateView, view)
    await validateOrReject(ViewPayload, { skipMissingProperties: true })
    const response = await this.$httpPut<ST.updateViewResponse>(`/v1/flow/${flowUuid}/view/${viewUuid}`, ViewPayload)
    return response?.data ?? null
  }

  /**
   *
   * @param flowUuid -
   * @param viewUuid -
   */
  public async findViewById(flowUuid: string, viewUuid: string) {
    const response = await this.$httpGet<ST.findViewByUuidResponse>(`/v1/flow/${flowUuid}/view/${viewUuid}`)
    return response?.data ?? null
  }

  /**
   *
   * @param flowUuid -
   * @param viewUuid -
   */
  public deleteViewById(flowUuid: string, viewUuid: string) {
    return this.$httpDelete<unknown>(`/v1/flow/${flowUuid}/view/${viewUuid}`)
  }

  /**
   *
   * @param flowUuid -
   * @param payload -
   */
  public async createBrand(flowUuid: string, brand: DAO.CreateBrand) {
    const BrandPayload = plainToClass(DTO.CreateBrand, brand)
    await validateOrReject(BrandPayload)
    const response = await this.$httpPost<ST.createBrandResponse>(`/v1/flow/${flowUuid}/brand`, BrandPayload)
    return response?.data?.brand ?? null
  }

  /**
   *
   * @param flowUuid -
   * @param brandUuid -
   * @param payload -
   */
  public async updateBrand(flowUuid: string, brandUuid: string, brand: DAO.UpdateBrand) {
    const BrandPayload = plainToClass(DTO.UpdateBrand, brand)
    await validateOrReject(BrandPayload, { skipMissingProperties: true })
    const response = await this.$httpPut<ST.updateBrandResponse>(
      `/v1/flow/${flowUuid}/brand/${brandUuid}`,
      BrandPayload
    )
    return response?.data ?? null
  }

  /**
   *
   * @param flowUuid -
   * @param viewUuid -
   * @param elementUuid -
   */
  public async findElementById(flowUuid: string, viewUuid: string, elementUuid: string) {
    const response = await this.$httpGet<ST.findElementByIdResponse>(
      `/v1/flow/${flowUuid}/view/${viewUuid}/element/${elementUuid}`
    )
    return response?.data ?? null
  }

  /**
   *
   * @param flowUuid -
   * @param viewUuid -
   * @param elementUuid -
   * @param element -
   */
  public async updateElementById(flowUuid: string, viewUuid: string, elementUuid: string, element: any) {
    //const ElementPayload = plainToClass(ElementDto, payload)
    //await validateOrReject(ElementPayload, { skipMissingProperties: true })
    const ElementPayload = element
    return this.$httpPut<ST.updateElementByIdResponse>(
      `/v1/flow/${flowUuid}/view/${viewUuid}/element/${elementUuid}`,
      ElementPayload
    )
  }

  /**
   *
   * @param flowUuid -
   * @param viewUuid -
   * @param element -
   */
  public async createElement(flowUuid: string, viewUuid: string, element: any) {
    //const ElementPayload = plainToClass(ElementDto, payload)
    //await validateOrReject(ElementPayload)
    const ElementPayload = element
    const response = await this.$httpPost<ST.createElementResponse>(`/v1/flow/${flowUuid}/view/${viewUuid}/element`, {
      elements: [ElementPayload]
    })
    return response?.data ?? null
  }

  /**
   *
   * @param flowUuid -
   * @param viewUuid -
   * @param elementUuid -
   */
  public deleteElement(flowUuid: string, viewUuid: string, elementUuid: string) {
    return this.$httpDelete<ST.deleteElementResponse>(`/v1/flow/${flowUuid}/view/${viewUuid}/element/${elementUuid}`)
  }

  /**
   *
   */
  public async findElementTypes() {
    const response = await this.$httpGet<ST.ElementTypesResponse>('/v1/utils/elements')
    return Object.keys(response?.data ?? {}) as ST.ElementType[]
  }

  /**
   *
   * @param flowId -
   * @param payload -
   */
  public async createDECBucket(flowId: string, bucket: DAO.CreateDECBucket) {
    const BucketPayload = plainToClass(DTO.CreateDECBucket, bucket)
    await validateOrReject(BucketPayload)
    return this.$httpPost<ST.createDECBucketResponse>(`/v1/flow/${flowId}/bucket`, {
      bucket_type: 'dec',
      configuration: BucketPayload
    })
  }
}
