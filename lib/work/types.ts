export type ElementType =
  | 'input_text'
  | 'input_dni'
  | 'dropdown'
  | 'radio_button'
  | 'button_success'
  | 'button_fail'
  | 'button_continue'
  | 'pdf_element'
  | 'check_input'
  | 'doble_dropdown'
  | 'label'
  | 'input_password'
  | 'input_email'
  | 'input_draw'
  | 'doble_dropdown_location'
  | 'small_label'
  | 'large_label'
  | 'hint_button'

export type ViewType = 'pdf_form' | 'custom_form' | 'empty_form' | 'normal_form'

export interface ServiceResponse<T> {
  code: number
  resource: string
  message: string
  data: T
}

export interface ChildAble {
  child_able: 'primary' | 'secondary' | null
  option_uuid: string | null
  father_uuid: string | null
}

export interface FatherAble {
  father_able: 'primary' | 'secondary' | null
}

export interface PreAble {
  pre_able: boolean | null
  pre_value: string | null
}

export interface Flow {
  _id: { $oid: string }
  uuid: string
  created_at: string
  name: string
  brand?: { description?: string }
}

export interface FlowDetails {
  _id: { $oid: string }
  brand?: BrandDetails
  company_id: string
  complete: boolean
  created_at: string
  deleted_at: string | null
  flavor_id: string
  name: string
  updated_at: string
  uuid: string
  views?: ViewDetails[]
}

export interface BrandDetails {
  _id: { $oid: string }
  created_at: string
  deleted_at: string | null
  description: string
  logo: string
  name: string
  primary_color: string
  updated_at: string
  uuid: string
}

export interface Answer {
  _id: { $oid: string }
  created_at: string
  trust_id: string
  uuid: string
}

export interface AnswerDetails {
  element_type: ElementType
  element_uuid: string
  value: string
}

export interface ElementDetails extends PreAble, FatherAble, ChildAble {
  _id: { $oid: string }
  created_at: string
  deleted_at: string | null
  element_type: ElementType
  label: string
  name: string
  options: ElementDetailsOption[]
  placeholder: string
  extra_placeholder: string
  order: number
  static: boolean
  required: boolean
  status: boolean
  updated_at: string
  uuid: string
  value: any
}

export interface ElementDetailsOption {
  _id: { $oid: string }
  uuid: string
  value: string
  description: string
  child?: Omit<ElementDetailsOption, 'child'>[]
}

export interface ViewDetails extends ChildAble {
  _id: { $oid: string }
  created_at: string
  deleted_at: string | null
  elements?: ElementDetails[]
  elements_name?: string[]
  code: string | number | null
  name: string
  status: boolean
  updated_at: string
  uuid: string
  view_type: ViewType
}

export type findFlowByIdResponse = ServiceResponse<FlowDetails>
export type findFlowsByFlavorIdResponse = ServiceResponse<Flow[]>
export type findFlowsByAppIdResponse = ServiceResponse<Flow[]>
export type createFlowResponse = ServiceResponse<FlowDetails>
export type updateFlowResponse = ServiceResponse<FlowDetails>
export type findAnswersByFlowIdResponse = ServiceResponse<Answer[]>
export type findAnswerByIdResponse = ServiceResponse<AnswerDetails[]>
export type createViewResponse = ServiceResponse<ViewDetails>
export type updateViewResponse = ServiceResponse<ViewDetails>
export type findViewByUuidResponse = ServiceResponse<ViewDetails>
export type createBrandResponse = ServiceResponse<{ brand: BrandDetails }>
export type updateBrandResponse = ServiceResponse<any>
export type findElementByIdResponse = ServiceResponse<ElementDetails>
export type updateElementByIdResponse = ServiceResponse<unknown>
export type createElementResponse = ServiceResponse<ViewDetails>
export type deleteElementResponse = ServiceResponse<unknown>
export type createDECBucketResponse = ServiceResponse<unknown>
export type ElementTypesResponse = ServiceResponse<
  {
    [key in ElementType]: number
  }
>
