import { IsString, IsUrl, IsOptional } from 'class-validator'
import { ST } from './index'
import { PartialType } from '@nestjs/swagger'

export class CreateView {
  @IsString()
  name!: string

  @IsOptional()
  @IsString()
  description!: string

  @IsString()
  view_type!: ST.ViewType

  @IsOptional()
  @IsString()
  code?: string
}

export class CreateBrand {
  @IsString()
  name!: string

  @IsOptional()
  @IsString()
  button_color!: string

  @IsString()
  primary_color!: string

  @IsOptional()
  @IsString()
  secondary_color!: string

  @IsOptional()
  @IsString()
  background_color!: string

  @IsOptional()
  @IsString()
  font!: string

  @IsString()
  @IsUrl()
  logo!: string

  @IsString()
  description!: string
}

export class CreateDECBucket {
  @IsString()
  user_rut!: string

  @IsString()
  user_pin!: string

  @IsString()
  user_institution!: string
}

export class UpdateView extends PartialType(CreateView) {}
export class UpdateBrand extends PartialType(CreateBrand) {}
