import { DTO } from './index'

export type CreateView = typeof DTO.CreateView.prototype
export type CreateBrand = typeof DTO.CreateBrand.prototype
export type CreateDECBucket = typeof DTO.CreateDECBucket.prototype
export type UpdateView = typeof DTO.UpdateView.prototype
export type UpdateBrand = typeof DTO.UpdateBrand.prototype
