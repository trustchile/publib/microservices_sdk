import * as DTO from './dto'

export type CreateCompany = typeof DTO.CreateCompany.prototype
export type UpdateCompany = typeof DTO.UpdateCompany.prototype
export type CreateApplication = typeof DTO.CreateApplication.prototype
