import { IsNotEmpty, IsString, Length, IsEmail, IsUrl, IsIn } from 'class-validator'
import { IsBundleId } from '../common/isBundleId'
import { PartialType } from '@nestjs/swagger'

export class CreateCompany {
  @IsNotEmpty()
  @IsString()
  @Length(1, 30)
  name!: string

  @IsString()
  @Length(0, 120)
  description!: string

  @IsNotEmpty()
  @IsEmail()
  email!: string

  @IsNotEmpty()
  @IsUrl()
  img!: string

  @IsNotEmpty()
  @IsString()
  @Length(1, 30)
  name_id!: string
}

export class CreateApplication {
  @IsNotEmpty()
  @IsString()
  company_id!: string

  @IsNotEmpty()
  @IsString()
  @Length(1, 30)
  name!: string

  @IsNotEmpty()
  @IsString()
  @Length(1, 120)
  description!: string

  @IsNotEmpty()
  @IsString()
  @IsBundleId()
  bundle_id!: string

  @IsNotEmpty()
  @IsString()
  flavor_id!: string

  @IsNotEmpty()
  @IsUrl()
  img!: string

  @IsNotEmpty()
  @IsString()
  @IsIn(['ios', 'android'])
  os!: 'ios' | 'android'
}

export class UpdateCompany extends PartialType(CreateCompany) {}
