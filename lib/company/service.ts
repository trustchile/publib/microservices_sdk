import { Singleton, HttpClient } from '../common'
import { ST, DTO, DAO } from './index'

/**
 *
 */
@Singleton()
export class Service extends HttpClient {
  /**
   *
   * @param company -
   */
  public createCompany(company: DAO.CreateCompany) {
    return this.$httpPost<ST.createCompanyResponse, DTO.CreateCompany>('/api/v1/company', company)
  }

  /**
   *
   * @param companyId -
   * @param company -
   */
  public updateCompany(companyId: string, company: DAO.UpdateCompany) {
    return this.$httpPut<ST.updateCompanyResponse, DTO.UpdateCompany>(`/api/v1/company/${companyId}`, company)
  }

  /**
   *
   * @param application -
   */
  public createApplication(application: DAO.CreateApplication) {
    return this.$httpPost<ST.createApplicationResponse, DTO.CreateApplication>('/api/v2/app', application)
  }

  /**
   *
   * @param companyId -
   */
  public async findCompanyById(companyId: number | string) {
    const response = await this.$httpGet<ST.findCompanyByIdResponse>(`/api/v1/company/${companyId}`)
    return response?.company ?? null
  }

  /**
   *
   */
  public async findAllCompanies() {
    const response = await this.$httpGet<ST.findAllCompaniesResponse>(`/api/v1/company`)
    return response?.companies ?? []
  }

  /**
   *
   * @param companyId -
   */
  public async findAllDevicesByCompanyId(companyId: number | string) {
    const response = await this.$httpGet<ST.findAllDevicesByCompanyIdResponse>(`/api/v1/company/${companyId}/device`)
    return response?.deviceInfo ?? []
  }

  /**
   *
   */
  public async findAllApps() {
    const response = await this.$httpGet<ST.findAllAppsResponse>(`/api/v1/app`)
    return response?.apps ?? []
  }

  /**
   *
   * @param appId -
   */
  public async findAppByAppId(appId: number | string) {
    const response = await this.$httpGet<ST.findAppByAppIdResponse>(`/api/v1/app/${appId}`)
    return response?.app ?? null
  }

  /**
   *
   * @param companyId -
   */
  public async findAppsByCompanyId(companyId: number | string) {
    const response = await this.$httpGet<ST.findAppsByCompanyIdResponse>(`/api/v1/company/${companyId}/app`)
    return response?.app ?? []
  }

  /**
   *
   * @param trustId -
   */
  public async findAppsByTrustId(trustId: number | string) {
    const response = await this.$httpGet<ST.findAppsByTrustIdResponse>(`/api/v1/device/${trustId}/app`)
    return response?.apps ?? []
  }

  /**
   *
   * @param dni -
   */
  public async findAppsByDni(dni: string) {
    const response = await this.$httpGet<ST.findAppsByDniResponse>(`/api/v1/directory/${dni}/app`)
    return response?.app ?? []
  }

  /**
   *
   * @param uuid -
   */
  public async findAppsByUuid(uuid: string) {
    const response = await this.$httpGet<ST.findAppsByUuidResponse>(`/api/v2/directory/${uuid}/app`)
    return response?.app ?? []
  }

  /**
   *
   * @param companyId -
   */
  public async findUsersAppsByCompanyId(companyId: number | string) {
    const response = await this.$httpGet<ST.findUsersAppsByCompanyIdResponse>(
      `/api/v1/company/${companyId}/directory/apps`
    )
    return response?.directory ?? []
  }

  /**
   *
   * @param appId -
   */
  public async findUsersAppsCountByAppId(appId: number | string): Promise<number> {
    const response = await this.$httpGet<ST.findUsersAppsCountByAppIdResponse>(`/api/v1/app/${appId}/directory/count`)
    return response?.count ?? 0
  }

  /**
   *
   * @param companyId -
   */
  public async findCompanyPermissionsByCompanyId(companyId: number | string) {
    const response = await this.$httpGet<ST.findCompanyPermissionsByCompanyIdResponse>(
      `/api/v1/company/${companyId}/permission`
    )
    return response?.permission ?? null
  }
}
