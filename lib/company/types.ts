export interface MinimumResponse {
  status: boolean
}

export interface CompanyDetails {
  id: number
  name: string
  description: string
  email: string
  img: string
  created_at: string
  updated_at: string
  status: boolean
  name_id: string
  industry: string | null
  country: string | null
  cause: string
  phone: string
}

export interface DeviceInformation {
  trust_id: string
  bundle_id: string
  flavor_id: string
  name: string
  description: string
  nickname: string
}

export interface ApplicationDetails {
  id: number
  bundle_id: string
  company_id: string
  name: string
  description: string
  created_at: string
  updated_at: string
  status: boolean
  img: string
  capabilities: any[]
  flavor_id: string
  os: null
  client_uid: null
}

export interface Permission {
  id: number
  company_id: number
  read: string[] | null
  write: string[] | null
  updated: string[] | null
  remove: string[] | null
  created_at: string
  updated_at: string
}

export type findAppsByCompanyIdResponse = MinimumResponse & { app: ApplicationDetails[] }
export type findAllAppsResponse = MinimumResponse & { apps: ApplicationDetails[] }
export type findAppsByTrustIdResponse = MinimumResponse & { apps: ApplicationDetails[] }
export type findAppsByDniResponse = MinimumResponse & { app: ApplicationDetails[] }
export type findAppsByUuidResponse = MinimumResponse & { app: ApplicationDetails[] }
export type findAppByAppIdResponse = MinimumResponse & { app: ApplicationDetails }
export type findAllDevicesByCompanyIdResponse = MinimumResponse & { deviceInfo: DeviceInformation[] }
export type findCompanyByIdResponse = MinimumResponse & { company: CompanyDetails }
export type findAllCompaniesResponse = MinimumResponse & { companies: CompanyDetails[] }
export type findUsersAppsByCompanyIdResponse = MinimumResponse & { directory: { dni: string; app_name: string[] }[] }
export type findUsersAppsCountByAppIdResponse = MinimumResponse & { count: number }
export type findCompanyPermissionsByCompanyIdResponse = MinimumResponse & { permission: Permission }
export type createCompanyResponse = any
export type updateCompanyResponse = any
export type createApplicationResponse = any
