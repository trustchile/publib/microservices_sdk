import * as GT from '../common/types'

export interface ServiceBaseResponse<T> {
  code: number
  resource: string
  message: string
  data: T
}

export interface Statistics {
  total: number
  sended?: number
  pending?: number
  fails?: number
  campaign: number
  actions?: StatisticActions
}

export interface StatisticActions {
  RECEIVED?: number
  CLOSED?: number
  PRESS_ACTION?: number
  UNKNOWN?: number
}

export type CampaignDetails = GT.TimestampAble & {
  app_id: string
  application_name: string
  company_id: string
  finish_date: string | null
  flavor_id: string
  message_id: string
  name: string
  segment_id: string
  statistics: Statistics
  terminated: boolean
  uuid: string
}

export type createCampaignForSegmentResponse = ServiceBaseResponse<CampaignDetails>
export type createCampaignForDevicesResponse = ServiceBaseResponse<CampaignDetails>
export type findStatisticsByCompanyIdResponse = ServiceBaseResponse<Statistics>
export type findCampaignsByCompanyIdResponse = ServiceBaseResponse<CampaignDetails[]> & { total: number }
export type findByIdResponse = ServiceBaseResponse<CampaignDetails>
