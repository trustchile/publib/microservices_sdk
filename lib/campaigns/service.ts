import { Singleton, HttpClient } from '../common'
import { ST } from './index'

/**
 *
 */
@Singleton()
export class Service extends HttpClient {
  /**
   *
   * @param companyId -
   * @param campaignName -
   * @param messageId -
   * @param appBundleId -
   * @param devices -
   */
  public async createCampaignForDevices(
    companyId: string | number,
    campaignName: string,
    messageId: string,
    appBundleId: string,
    devices: string[]
  ) {
    const response = await this.$httpPost<ST.createCampaignForDevicesResponse>('/api/v1/campaign', {
      name: campaignName,
      message_id: messageId,
      application_name: appBundleId,
      company_id: String(companyId),
      notifications: devices.map((device) => ({ trust_id: device, variables: {} }))
    })

    return response?.data ?? null
  }

  /**
   *
   * @param companyId -
   * @param campaignName -
   * @param messageId -
   * @param applicationBundleId -
   * @param segmentId -
   * @param campaignEndDate -
   */
  public async createCampaignForSegment(
    companyId: string | number,
    campaignName: string,
    messageId: string,
    appBundleId: string,
    segmentId: string,
    campaignEndDate?: string
  ) {
    const response = await this.$httpPost<ST.createCampaignForSegmentResponse>('/api/v1/campaign', {
      name: campaignName,
      message_id: messageId,
      application_name: appBundleId,
      company_id: String(companyId),
      segment_id: segmentId,
      finish_date: campaignEndDate
    })

    return response?.data ?? null
  }

  /**
   *
   * @param companyId -
   */
  public async findStatisticsByCompanyId(companyId: string | number) {
    const response = await this.$httpGet<ST.findStatisticsByCompanyIdResponse>(
      `/api/v1/company/${companyId}/statistics`
    )

    return response?.data ?? null
  }

  /**
   *
   * @param companyId -
   * @param terminated -
   * @param fields -
   * @param params -
   */
  public findAllByCompanyId(
    companyId: string | number,
    fields: string[] = [],
    params: { page?: number; per_page?: number; search?: string; terminated?: boolean } = {
      page: 1,
      per_page: 5,
      search: '',
      terminated: false
    }
  ) {
    return this.$httpGet<ST.findCampaignsByCompanyIdResponse>(`/api/v1/company/${companyId}/campaign`, {
      fields: fields.join(','),
      ...params
    })
  }

  /**
   *
   * @param uuid -
   * @param fields -
   */
  public async findById(uuid: string, fields: string[] = []) {
    const response = await this.$httpGet<ST.findByIdResponse>(`/api/v1/campaign/${uuid}`, {
      fields: fields.join(',')
    })

    return response?.data ?? null
  }
}
