import { Service } from './service'

export type MessageType = 'notification' | 'dialog' | 'banner' | 'video'
export type MessageKey = 'notificationDialog' | 'notificationVideo' | 'notificationBody'
export type PlayTime = '5' | '10' | '15'

export type OpenAppAble = {
  openApp: boolean
}

export type LandscapeAble = {
  landscape: boolean
}

export interface ServiceResponse<T> {
  status: boolean
  message: string
  notification: T
}

export interface CustomNotification {
  data: StoredMessageTypeDialogAndBanner | StoredMessageTypeNotification | StoredMessageTypeVideo
  apns: APNSWrapper
}

export type MessagePersistenceUuidAble = {
  message_persistence_uuid: string
}

export type StoredMessageTypeDialogAndBanner = MessagePersistenceUuidAble & {
  type: 'dialog' | 'banner'
  notificationDialog: string | MessageTypeDialogWithCommonButtons | MessageTypeBannerWithCommonButtons
}

export type StoredMessageTypeNotification = MessagePersistenceUuidAble & {
  type: 'notification'
  notificationBody: string | MessageTypeNotificationWithAllButtons
}

export type StoredMessageTypeVideo = MessagePersistenceUuidAble & {
  type: 'video'
  notificationVideo: string | MessageTypeVideoWithCommonButtons
}

export type ButtonAble<T> = {
  buttons: T[]
}

export type ImageUrlAble = {
  image_url: string
}

export type Cancelable = {
  isCancelable: string
}

export type PersistentAble = {
  isPersistent: string
}

export type TextBodyAble = {
  text_body: string
}

export type MessageTypeNotification = ImageUrlAble &
  TextBodyAble & {
    text_title: string
  }

export type MessageTypeBanner = ImageUrlAble & PersistentAble & Cancelable
export type MessageTypeDialog = ImageUrlAble & TextBodyAble & PersistentAble & Cancelable
export type MessageTypeVideo = PersistentAble & {
  video_url: string
  isSound: string
  portrait: boolean
  autoPlay: boolean
  min_play_time: PlayTime
}

export type MessageTypeNotificationWithCommonButtons = MessageTypeNotification & ButtonAble<CommonButton>
export type MessageTypeNotificationWithOpenAppButton = MessageTypeNotification & ButtonAble<OpenAppButton>
export type MessageTypeNotificationWithAllButtons = MessageTypeNotification & ButtonAble<CommonButton | OpenAppButton>
export type MessageTypeNotificationOpenAppAble = MessageTypeNotification & OpenAppAble
export type MessageTypeNotificationOpenAppAbleWithCommonButtons = MessageTypeNotification &
  OpenAppAble &
  ButtonAble<CommonButton>
export type MessageTypeVideoLandscapeAble = MessageTypeVideo & LandscapeAble
export type MessageTypeVideoLandscapeAbleWithCommonButtons = MessageTypeVideo & LandscapeAble & ButtonAble<CommonButton>
export type MessageTypeBannerWithCommonButtons = MessageTypeBanner & ButtonAble<CommonButton>
export type MessageTypeDialogWithCommonButtons = MessageTypeDialog & ButtonAble<CommonButton>
export type MessageTypeVideoWithCommonButtons = MessageTypeVideo & ButtonAble<CommonButton>

export interface APNSWrapper {
  payload: any
  data: CustomNotification['data']
}

export type OpenAppButton = {
  type: 'openapp'
}

export type CommonButton = {
  type: 'call' | 'deeplink' | 'url'
  enabled: boolean
  text: string
  action: string
  color: string
}

export type AllButton = OpenAppButton | CommonButton

export interface NotificationResponse {
  id: number
  uuid: string
  custom_notification: CustomNotification
  values: any[]
  created_at: string
  updated_at: string
  status: boolean
  type_notification: 'trust_notification'
  disposable: boolean
}

export type findOneByIdResponse = ServiceResponse<NotificationResponse>
export type MessageToTransform = CustomNotification['data']
export type MessageToParse = ReturnType<typeof Service.prototype.transform>
export type createOneResponse = { message: string; status: boolean; uuid: string }
export type findOneByIdAndUpdateResponse = {
  code: number
  resource: string
  message: string
  data: NotificationResponse
}
