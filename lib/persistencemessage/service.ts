import { Singleton, HttpClient } from '../common'
import { ST } from './index'
import Omit from 'lodash/omit'
import Pick from 'lodash/pick'
import Defaults from 'lodash/defaultsDeep'
import Update from 'lodash/update'
import Tap from 'lodash/tap'
import Thru from 'lodash/thru'

const L = { Omit, Pick, Defaults, Update, Tap, Thru }

function safeJSONParse<T = any>(source: string | T, alternative: T | any = null): T | any {
  if (typeof source === 'string') {
    try {
      return JSON.parse(source)
    } catch (error) {
      return alternative
    }
  } else if (typeof source === 'object' && source !== null) {
    return source
  } else {
    return alternative
  }
}

/**
 *
 */
@Singleton()
export class Service extends HttpClient {
  private readonly MESSAGE_DATA_BUTTONS_DEFAULT: ST.CommonButton[] = [
    {
      enabled: false,
      action: '',
      color: '#000000',
      text: '',
      type: '' as any
    }
  ]
  private readonly MESSAGE_TYPE_NOTIFICATION_DEFAULT: ST.MessageTypeNotificationWithAllButtons = {
    buttons: [...this.MESSAGE_DATA_BUTTONS_DEFAULT],
    image_url: '',
    text_body: '',
    text_title: ''
  }

  private readonly MESSAGE_TYPE_NOTIFICATION_OPENAPPABLE_DEFAULT: ST.MessageTypeNotificationOpenAppAbleWithCommonButtons = {
    ...(this.MESSAGE_TYPE_NOTIFICATION_DEFAULT as ST.MessageTypeNotificationWithCommonButtons),
    openApp: false
  }

  private readonly MESSAGE_TYPE_DIALOG_DEFAULT: ST.MessageTypeDialogWithCommonButtons = {
    buttons: [...this.MESSAGE_DATA_BUTTONS_DEFAULT],
    image_url: '',
    text_body: '',
    isPersistent: 'true',
    isCancelable: 'true'
  }

  private readonly MESSAGE_TYPE_BANNER_DEFAULT: ST.MessageTypeBannerWithCommonButtons = {
    buttons: [...this.MESSAGE_DATA_BUTTONS_DEFAULT],
    image_url: '',
    isPersistent: 'true',
    isCancelable: 'true'
  }

  private readonly MESSAGE_TYPE_VIDEO_DEFAULT: ST.MessageTypeVideoWithCommonButtons = {
    buttons: [...this.MESSAGE_DATA_BUTTONS_DEFAULT],
    video_url: '',
    isSound: 'false',
    portrait: true,
    autoPlay: true,
    min_play_time: '5' as ST.PlayTime,
    isPersistent: 'true'
  }

  private readonly MESSAGE_TYPE_VIDEO_LANDSCAPEABLE_DEFAULT: ST.MessageTypeVideoLandscapeAble = {
    ...this.MESSAGE_TYPE_VIDEO_DEFAULT,
    landscape: !this.MESSAGE_TYPE_VIDEO_DEFAULT.portrait
  }

  private readonly MESSAGE_TYPE_DIALOG_KEYS = Object.keys(this.MESSAGE_TYPE_DIALOG_DEFAULT)
  private readonly MESSAGE_TYPE_BANNER_KEYS = Object.keys(this.MESSAGE_TYPE_BANNER_DEFAULT)
  private readonly MESSAGE_TYPE_VIDEO_KEYS = Object.keys(this.MESSAGE_TYPE_VIDEO_DEFAULT)
  private readonly MESSAGE_TYPE_NOTIFICATION_KEYS = Object.keys(this.MESSAGE_TYPE_NOTIFICATION_DEFAULT)

  /**
   *
   * @param uuid - Message UUID
   */
  public async findOneById(uuid: string) {
    const response = await this.$httpGet<ST.findOneByIdResponse>(`/api/notification/${uuid}`)
    return { ...response.notification, notification: this.transform(response.notification.custom_notification.data) }
  }

  /**
   *
   * @param payload -
   */
  public async createOne(payload: ST.MessageToParse) {
    const response = await this.$httpPost<ST.createOneResponse>('/api/create', {
      message: this.parse(payload),
      values: []
    })
    return response?.uuid ?? ''
  }

  /**
   *
   * @param uuid -
   */
  public findOneByIdAndDelete(uuid: string) {
    return this.$httpPost(`/api/delete`, {
      uuid,
      status: 'false'
    })
  }

  /**
   *
   * @param uuid -
   * @param message -
   */
  public findOneByIdAndUpdate(uuid: string, message: ST.MessageToParse) {
    return this.$httpPost<ST.findOneByIdAndUpdateResponse>(`/api/edit/${uuid}`, this.parse(message))
  }

  /**
   * Transforms from $PersistenceMessage#`message` to Frontend#`message`
   * @param message -
   * @internal
   */
  public transform(message?: ST.MessageToTransform) {
    if (!message || (message && !['notification', 'banner', 'dialog', 'video'].includes(message.type))) {
      return null
    }

    return message.type === 'notification'
      ? {
          type: 'notification' as const,
          data: <ST.MessageTypeNotificationOpenAppAbleWithCommonButtons>L.Defaults(
            L.Thru<ST.MessageTypeNotificationWithAllButtons, ST.MessageTypeNotificationOpenAppAbleWithCommonButtons>(
              safeJSONParse(message.notificationBody, this.MESSAGE_TYPE_NOTIFICATION_OPENAPPABLE_DEFAULT),
              ({ buttons: messageButtons = [], ...message }) => ({
                ...message,
                buttons: messageButtons.filter((button) => button.type !== 'openapp') as ST.CommonButton[],
                openApp: messageButtons.some((button) => button.type === 'openapp')
              })
            ),
            this.MESSAGE_TYPE_NOTIFICATION_OPENAPPABLE_DEFAULT
          )
        }
      : message.type === 'banner'
      ? {
          type: 'banner' as const,
          data: <ST.MessageTypeBannerWithCommonButtons>L.Defaults(
            L.Thru<ST.MessageTypeBannerWithCommonButtons, ST.MessageTypeBannerWithCommonButtons>(
              safeJSONParse(message.notificationDialog, this.MESSAGE_TYPE_BANNER_DEFAULT),
              ({ buttons: messageButtons = [], ...message }) => ({
                ...message,
                buttons: messageButtons
              })
            ),
            this.MESSAGE_TYPE_BANNER_DEFAULT
          )
        }
      : message.type === 'dialog'
      ? {
          type: 'dialog' as const,
          data: <ST.MessageTypeDialogWithCommonButtons>L.Defaults(
            L.Thru<ST.MessageTypeDialogWithCommonButtons, ST.MessageTypeDialogWithCommonButtons>(
              safeJSONParse(message.notificationDialog, this.MESSAGE_TYPE_DIALOG_DEFAULT),
              ({ buttons: messageButtons = [], ...message }) => ({
                ...message,
                buttons: messageButtons
              })
            ),
            this.MESSAGE_TYPE_DIALOG_DEFAULT
          )
        }
      : message.type === 'video'
      ? {
          type: 'video' as const,
          data: <ST.MessageTypeVideoLandscapeAbleWithCommonButtons>L.Defaults(
            L.Thru<
              ST.MessageTypeVideoLandscapeAbleWithCommonButtons,
              ST.MessageTypeVideoLandscapeAbleWithCommonButtons
            >(
              safeJSONParse(message.notificationVideo, this.MESSAGE_TYPE_VIDEO_LANDSCAPEABLE_DEFAULT),
              ({ buttons: messageButtons = [], portrait = true, ...message }) => ({
                ...message,
                buttons: messageButtons,
                landscape: !portrait,
                portrait
              })
            ),
            this.MESSAGE_TYPE_VIDEO_LANDSCAPEABLE_DEFAULT
          )
        }
      : null
  }

  /**
   * Parses from Frontend#`message` to $PersistenceMessage#`message`
   * @param message -
   * @internal
   */
  public parse(message?: ST.MessageToParse) {
    if (!message) {
      return null
    }

    if (!message.data.buttons) {
      message.data.buttons = []
    }

    /**
     * @privateRemarks - Returns something like: Object:: type: MessageType, [MessageKey]: Original Data "then" Modify "then" Fill missing keys from Shchema "then" Whitelist from Schema "then" Stringify::
     */
    const data =
      message.type === 'notification'
        ? ({
            type: 'notification',
            notificationBody: JSON.stringify(
              L.Pick(
                L.Defaults(
                  L.Thru(message.data, ({ buttons: messageButtons = [], openApp = false, ...message }) => ({
                    ...message,
                    buttons: L.Thru<ST.AllButton[], ST.AllButton[]>(
                      [...((openApp ? [{ type: 'openapp' }] : []) as ST.AllButton[])].concat(
                        messageButtons.filter((button) => button.enabled)
                      ),
                      (newButtons) => (newButtons.length > 0 ? newButtons : this.MESSAGE_DATA_BUTTONS_DEFAULT)
                    )
                  })),
                  this.MESSAGE_TYPE_NOTIFICATION_DEFAULT
                ),
                this.MESSAGE_TYPE_NOTIFICATION_KEYS
              )
            )
          } as const)
        : message.type === 'banner'
        ? ({
            type: 'banner',
            notificationDialog: JSON.stringify(
              L.Pick(
                L.Defaults(
                  L.Thru(message, (message) => {
                    const newButtons = message.data.buttons.filter((button) => button.enabled)
                    message.data.buttons = newButtons.length > 0 ? newButtons : this.MESSAGE_DATA_BUTTONS_DEFAULT
                    return message.data
                  }),
                  this.MESSAGE_TYPE_BANNER_DEFAULT
                ),
                this.MESSAGE_TYPE_BANNER_KEYS
              )
            )
          } as const)
        : message.type === 'dialog'
        ? ({
            type: 'dialog',
            notificationDialog: JSON.stringify(
              L.Pick(
                L.Defaults(
                  L.Thru(message, (message) => {
                    const newButtons = message.data.buttons.filter((button) => button.enabled)
                    message.data.buttons = newButtons.length > 0 ? newButtons : this.MESSAGE_DATA_BUTTONS_DEFAULT
                    return message.data
                  }),
                  this.MESSAGE_TYPE_DIALOG_DEFAULT
                ),
                this.MESSAGE_TYPE_DIALOG_KEYS
              )
            )
          } as const)
        : message.type === 'video'
        ? ({
            type: 'video',
            notificationVideo: JSON.stringify(
              L.Pick(
                L.Defaults(
                  L.Thru(message, (message) => {
                    const newButtons = message.data.buttons.filter((button) => button.enabled)
                    message.data.buttons = newButtons.length > 0 ? newButtons : this.MESSAGE_DATA_BUTTONS_DEFAULT
                    message.data.portrait = 'landscape' in message.data ? !message.data.landscape : false
                    return message.data
                  }),
                  this.MESSAGE_TYPE_VIDEO_DEFAULT
                ),
                this.MESSAGE_TYPE_VIDEO_KEYS
              )
            )
          } as const)
        : ({
            type: 'notification',
            notificationBody: JSON.stringify(this.MESSAGE_TYPE_NOTIFICATION_DEFAULT)
          } as const)

    return {
      data,
      apns: {
        payload: {
          data,
          'url-scheme': 'https://google.com',
          aps: {
            'mutable-content': 1,
            sound: 'default',
            category: 'url-call',
            badge: 0,
            alert: {
              title: message.type === 'notification' ? message.data.text_title : 'Tienes un nuevo mensaje',
              body:
                message.type === 'notification' || message.type === 'dialog'
                  ? message.data.text_body
                  : 'Toca aquí para poder verlo'
            }
          }
        }
      }
    }
  }
}
