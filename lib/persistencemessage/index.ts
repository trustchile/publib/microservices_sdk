export * from './service'
export * as ST from './types'
export * as DTO from './dto'

import { Service } from './service'

export const instance = new Service()
