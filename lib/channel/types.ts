export interface Counter {
  created_at: Date
  updated_at: string
  like_count: number
  resource_id: string
  resource_name: string
}

export interface ServiceBaseResponse<T> {
  message: string
  data: T
}

export type findMessageStatusByIdResponse = ServiceBaseResponse<Counter>
