import { Singleton, HttpClient } from '../common'
import { ST } from './index'

/**
 * Clase que representa el servicio de "Channel" en Trust Technologies SpA
 * Ravencourt - jaimeferreiragomez\@gmail.com
 */
@Singleton()
export class Service extends HttpClient {
  /**
   *
   * @param messageId -
   */
  public async findMessageStatusById(messageId: string) {
    const response = await this.$httpGet<ST.findMessageStatusByIdResponse>(
      `/v1/message_status/TRUST_NOTIFICATION/${messageId}`
    )
    return response?.data ?? null
  }
}
