import { IsArray, IsHexColor, IsInstance, IsString } from 'class-validator'

export class AddTagToApp {
  @IsString()
  tag_name!: string

  @IsString()
  description!: string

  @IsHexColor()
  color!: string

  @IsArray()
  values!: string[]
}

export class AddTagsToIdentity {
  @IsString()
  dni!: string

  @IsInstance(Map)
  @IsString({ each: true })
  tags!: Map<string, string>
}
