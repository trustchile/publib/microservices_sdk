import { ST as SPT } from '../persistencesegment/index'
import { Singleton, HttpClient, GT } from '../common'
import { ST, DTO, DAO } from './index'

/**
 *
 */
@Singleton()
export class Service extends HttpClient {
  /**
   *
   * @param searchData -
   * @param filterFields -
   * @param paginationQuery -
   */
  public search<T = ST.DefaultSearchFields>(
    searchData: { [key: string]: string[] | string } | SPT.IndexQuery,
    filterFields: string[] = [],
    paginationQuery: GT.IPagination = { page: 1, per_page: 1000000000 }
  ) {
    return this.$httpPost<ST.SearchResponse<T>>('/v1/device', searchData, {
      ...paginationQuery,
      fields: filterFields.join(',')
    })
  }

  /**
   *
   * @param appId -
   */
  public async getDevicesCountByApplicationId(appId: string | number) {
    const response = await this.search({ 'apps.app_id': [String(appId)] }, undefined, { page: 1, per_page: 1 })
    return response?.total
  }

  /**
   *
   * @param appId -
   */
  public async getAppVersions(appId: string | number) {
    const response = await this.$httpGet<ST.getAppVersionsResponse>(`/v1/app/${appId}/versions`)
    return response?.data ?? []
  }

  /**
   *
   * @param appId -
   * @param version -
   */
  public async getAppVersionCount(appId: string | number, version: string) {
    const response = await this.$httpGet<ST.getAppVersionCountResponse>(
      `/v1/app/${appId}/versions/count?app_version=${version}`
    )
    return response?.data ?? 0
  }

  /**
   *
   * @param systemName -
   */
  public async getDevicesBrandsBySystemNames(systemNames: string[] = []) {
    const response = await this.$httpGet<ST.getDevicesBrandsByQueryResponse>(`/v1/device/brands`, {
      system_name: systemNames
    })
    return response?.data ?? []
  }

  /**
   *
   * @param appId -
   * @param paginationQuery -
   */
  public async getIdentitiesTags(appId: string, paginationQuery: GT.IPagination = { page: 1, per_page: 1000000000 }) {
    const response = await this.$httpGet<ST.getIdentitiesTagsResponse>(
      `/v1/app/${appId}/persons/tags/query`,
      paginationQuery
    )
    return response?.data ?? []
  }

  /**
   *
   * @param companyId -
   * @param identityDni -
   */
  public async getIdentitySystemicTagsByCompanyId(companyId: string | number, identityDni: string) {
    try {
      const response = await this.$httpGet<ST.getIdentitySystemicTagsByCompanyIdResponse>(
        `/v1/company/${companyId}/persons/tags`,
        { dni: identityDni }
      )

      return response.data.tags.systemic_sub_company ?? ''
    } catch (e) {
      return ''
    }
  }

  /**
   *
   * @param systemName -
   * @param brands -
   */
  public async getDevicesModelsByBrands(systemName: string[] = [], brands: string[] = []) {
    const response = await this.$httpGet<ST.getDevicesModelsByQueryResponse>(`/v1/device/models`, {
      system_name: systemName,
      brand: brands
    })
    return response?.data ?? []
  }

  /**
   *
   * @param trustId -
   */
  public async getDeviceByTrustId(trustId: string, fields: string[]) {
    const response = await this.$httpGet<ST.getDeviceByTrustIdResponse>(`/v1/device/${trustId}`, {
      fields: fields.join(', ')
    })
    return response?.data ?? null
  }

  /**
   *
   * @param appId -
   */
  public async getApplicationTags(appId: number | string) {
    const response = await this.$httpGet<ST.getApplicationTagsResponse>(`/v1/app/${appId}/tags`)
    return response?.data.tags ?? []
  }

  /**
   *
   * @param appId -
   * @param tag -
   */
  public async addTagToApplication(appId: number | string, tag: DAO.AddTagToApp) {
    const response = await this.$httpPost<ST.getApplicationTagsResponse, DTO.AddTagToApp>(`/v1/app/${appId}/tags`, tag)
    return response?.message ?? ''
  }

  /**
   *
   * @param appId -
   * @param identityDni -
   * @param tags -
   */
  public async addTagsToApplicationIdentity(appId: number | string, identityDni: string, tags: DAO.AddTagsToIdentity) {
    const response = await this.$httpPost<ST.addTagsToApplicationIdentityResponse, DTO.AddTagsToIdentity>(
      `/v1/app/${appId}/persons/tags`,
      { dni: identityDni, tags: Object.fromEntries(tags) }
    )
    return response?.message ?? ''
  }

  /**
   *
   * @param appId -
   * @param tagKey -
   */
  public async deleteApplicationTag(appId: string | number, tagKey: string) {
    const response = await this.$httpDelete<ST.deleteApplicationTagResponse>(`/v1/app/${appId}/tags/${tagKey}`)
    return response?.message ?? ''
  }

  /**
   *
   * @param companyId -
   */
  public async getSystemicTagsInCompanyById(companyId: string | number) {
    const response = await this.$httpGet<ST.getSystemicTagValuesByCompanyIdResponse>(
      `/v1/company/${companyId}/systemic_tag_value`
    )
    return response?.data ?? []
  }

  /**
   *
   * @param companyId -
   * @param group -
   */
  public async createSystemicTagInCompanyById(companyId: string | number, group: ST.Group) {
    const response = await this.$httpPost<ST.createSystemicTagValueInCompanyResponse>(
      `/v1/company/${companyId}/systemic_tag_value`,
      group
    )
    return response?.data ?? null
  }

  /**
   *
   * @param groupId -
   */
  public removeSystemicTag(groupId: string) {
    return this.$httpDelete(`/v1/systemic_tag_value/${groupId}`)
  }

  /**
   *
   * @param companyId -
   * @param identityDni -
   * @param groupId -
   */
  public addSystemicTagToIdentityByCompanyId(companyId: string | number, identityDni: string, groupId: string) {
    return this.$httpPost(`/v1/company/${companyId}/persons/tags`, {
      dni: identityDni,
      tags: {
        systemic_sub_company: groupId
      }
    })
  }

  /**
   *
   * @param groupId -
   * @param group -
   */
  public updateSystemicTag(groupId: string, group: Omit<Partial<ST.Group>, 'systemic_tag_name'>) {
    return this.$httpPut(`/v1/systemic_tag_value/${groupId}`, group)
  }
}
