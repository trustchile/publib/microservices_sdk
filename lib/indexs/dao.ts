import * as DTO from './dto'

export type AddTagToApp = typeof DTO.AddTagToApp.prototype
export type AddTagsToIdentity = typeof DTO.AddTagsToIdentity.prototype.tags
