import * as GT from '../common/types'

export interface DefaultSearchFields {
  apps: {
    app_id: number
    created_at: string
    updated_at: string
  }[]
  brand: string
  created_at: string
  emulator: boolean
  model: string
  os_version: string
  system_name: string
  trust_id: string
  updated_at: Date
}

export interface Group {
  name: string
  description: string
  color: string
  systemic_tag_name: 'systemic_sub_company'
}

export interface MinimumResponse<T> {
  code: number
  resource: string
  data: T
  message: string
}

export interface SearchResponse<T> {
  code: number
  resources: 'device'
  data: T[]
  message: string
  page: number
  per_page: number
  total: number
}

export interface Tag extends GT.IdAble {
  color: string
  description: string
  name: string
  updated_at: string
  values: string[]
}

export interface Bulk {
  email?: string
  dni?: string
  tags: {
    [tagName: string]: string
  }
}

export interface BrandQueryResponseData {
  system_name: string
  brands: string[]
}

export interface ModelQueryResponseData {
  model: string
  brands: string[]
}

export interface TagsByPersonQueryResponseData extends GT.TimestampAble {
  application_id: string
  dni: string
  tags: GT.Anything
}

export interface DeviceResponseData extends GT.TimestampAble {
  brand: string
  emulator: boolean
  imei: string
  location?: [number, number]
  model: string
  system_name: string
  system_version: string
  trust_id: string
  apps: Array<
    {
      app_id: string
      tags: GT.Anything
      version: null
      person?: {
        dni: string
        email: string
        name: string
        updated_at: string
      }
    } & GT.TimestampAble
  >
}

export interface ApplicationTagsResponseData extends GT.TimestampAble {
  application_id: string
  tags: Tag[]
}

export interface SystemicTag {
  color: string
  systemic_tag_name: 'systemic_sub_company'
  company_id: string
  created_at: string
  name: string
  updated_at: string
  uuid: string
}

export type getIdentitySystemicTagsByCompanyIdResponse = MinimumResponse<{
  company_uid: string
  created_at: string
  dni: string
  updated_at: string
  tags: { systemic_sub_company: string }
}>
export type createSystemicTagValueInCompanyResponse = MinimumResponse<SystemicTag>
export type getSystemicTagValuesByCompanyIdResponse = MinimumResponse<SystemicTag[]>
export type deleteApplicationTagResponse = MinimumResponse<string>
export type addTagsToApplicationIdentityResponse = MinimumResponse<TagsByPersonQueryResponseData>
export type getAppVersionsResponse = MinimumResponse<string[]>
export type getAppVersionCountResponse = MinimumResponse<number>
export type getDevicesBrandsByQueryResponse = MinimumResponse<BrandQueryResponseData[]>
export type getDevicesModelsByQueryResponse = MinimumResponse<ModelQueryResponseData[]>
export type getDeviceByTrustIdResponse = MinimumResponse<DeviceResponseData>
export type getApplicationTagsResponse = MinimumResponse<ApplicationTagsResponseData>
export type getIdentitiesTagsResponse = GT.PaginatedServiceBaseResponse &
  MinimumResponse<TagsByPersonQueryResponseData[]>
