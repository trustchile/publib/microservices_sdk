import { Singleton, HttpClient } from '../common'
import { ST } from './index'

/**
 *
 */
@Singleton()
export class Service extends HttpClient {
  /**
   *
   * @param searchData -
   * @param filterFields -
   * @param auditQuery -
   */
  public search<T = ST.DefaultSearchFields>(
    searchData: { [key: string]: string[] },
    filterFields: string[] = ['application', 'source', 'created_at'],
    auditQuery: ST.Query = { page: 1, per_page: 1 }
  ) {
    return this.$httpPost<ST.ServiceBaseResponse<T>>('/api/v1/audit/search', searchData, {
      ...auditQuery,
      fields: filterFields.join(',')
    })
  }

  /**
   *
   * @param trustId -
   * @param param1 -
   */
  public async findAuditsByTrustId(trustId: string, { per_page = 10, page = 1 }: any) {
    const data = await this.search({ 'source.trust_id': [trustId] }, [], { per_page, page })
    const total = Number(data?.total ?? 1)
    return {
      perPage: per_page,
      currentPage: page,
      totalPages: total,
      totalItems: total * 10,
      pageItems: data?.audit ?? []
    }
  }
}
