import * as GT from '../common/types'

export interface Query {
  page?: number
  per_page?: number
  start_date?: string
  end_date?: string
}

export interface DefaultSearchFields {
  application: string
  created_at: string
  source: AuditSource
}

export interface AuditSource {
  connection_type: string
  os: string
  os_version: string
  trust_id: string
  app_name: string
  bundle_id: string
  device_name: string
  connection_name: string
  version_app: string
  lonGeo: string
  latGeo: string
}

export interface ServiceBaseResponse<T> {
  status: boolean
  audit: Audit<T>[]
  page: number
  per_page: number
  total: number
}

export type Audit<T> = T & GT.IdAble
