import { IsNotEmpty, IsEmail, IsString, Length, IsOptional, IsBoolean } from 'class-validator'
import { Role } from './types'

export class CreateUser {
  @IsNotEmpty()
  @IsString()
  @IsEmail()
  email!: string

  @IsNotEmpty()
  @IsString()
  @Length(1, 30)
  name!: string

  @IsNotEmpty()
  @IsString()
  @Length(1, 30)
  last_name!: string

  @IsNotEmpty()
  @IsString()
  @Length(10, 11)
  dni!: string

  @IsNotEmpty()
  @IsString()
  company_uid!: string

  @IsNotEmpty()
  @IsString({ each: true })
  role!: Role[]

  @IsOptional()
  img?: string

  @IsOptional()
  @IsBoolean()
  invitation?: boolean

  @IsOptional()
  @IsString()
  password?: string

  @IsOptional()
  @IsString()
  load_uuid?: string
}

export class UpdateUser {}
