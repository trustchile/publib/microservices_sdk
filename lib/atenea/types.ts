export interface User {
  company_uid: string
  created_at: string
  dni: string
  email: string
  id: number
  img: string
  last_name: string
  name: string
  nationality: string
  phone_number: string
  status: boolean
  updated_at: string
}

export type Role =
  | 'trust_admin'
  | 'enrollment_super_admin'
  | 'enrollment_admin'
  | 'enrollment_user'
  | 'noti_super_admin'
  | 'noti_admin'
  | 'noti_user'

export interface MinimumResponse {
  status: boolean
  message: string
}

export type findUsersResponse = MinimumResponse & { total: number; user: User[] }
export type createUserResponse = unknown
export type findRolesByUserIdResponse = { roles: { id: number; name: Role }[] }
export type addRoleToUserResponse = unknown
export type removeRoleFromUserResponse = unknown
export type findUserByIdResponse = { user: User }
export type updateUserByIdResponse = unknown
export type disableUserByIdResponse = unknown
export type enableUserByIdResponse = unknown
export type resendUserConfirmationEmailResponse = unknown
