import { Singleton, HttpClient } from '../common'
import { ST, DTO, DAO } from './index'

/**
 *
 */
@Singleton()
export class Service extends HttpClient {
  /**
   *
   * @param pagination -
   */
  public async findUsers(
    pagination: { per_page: number; page: number; search: string; status?: boolean } = {
      per_page: 5,
      page: 1,
      search: ''
    }
  ) {
    const fields = ['id', 'email', 'company_uid', 'name', 'last_name', 'dni', 'img', 'status'].join(', ')
    return await this.$httpGet<ST.findUsersResponse>('/users', { ...pagination, fields })
  }

  /**
   *
   * @param user -
   */
  public async createUser(user: DAO.CreateUser) {
    const { roles, ...userRestData } = user
    return await this.$httpPost<ST.createUserResponse, DTO.CreateUser>('/users/create', {
      ...userRestData,
      role: roles
    })
  }

  /**
   *
   * @param userId -
   */
  public async findRolesByUserId(userId: string | number) {
    const response = await this.$httpGet<ST.findRolesByUserIdResponse>(`/users/${userId}/role`)
    return response?.roles ?? []
  }

  /**
   *
   * @param userId -
   * @param role -
   */
  public addRoleToUser(userId: string | number, role: ST.Role) {
    return this.$httpPost<ST.addRoleToUserResponse>(`/users/${userId}/role/${role}`, undefined)
  }

  /**
   *
   * @param userId -
   * @param role -
   */
  public removeRoleFromUser(userId: string | number, role: ST.Role) {
    return this.$httpDelete<ST.removeRoleFromUserResponse>(`/users/${userId}/role/${role}`)
  }

  /**
   *
   * @param userId -
   */
  public async findUserById(userId: string | number) {
    const response = await this.$httpGet<ST.findUserByIdResponse>(`/users/id/${userId}`)
    return response?.user ?? null
  }

  /**
   *
   * @param userId -
   * @param payload -
   */
  public updateUserById(userId: string | number, user: DAO.UpdateUser) {
    return this.$httpPut<ST.updateUserByIdResponse, DTO.UpdateUser>(`/users/${userId}`, user)
  }

  /**
   *
   * @param userId -
   */
  public disableUserById(userId: string | number) {
    return this.$httpPut<ST.disableUserByIdResponse>(`/users/disable/${userId}`, {})
  }

  /**
   *
   * @param userId -
   */
  public enableUserById(userId: string | number) {
    return this.$httpPut<ST.enableUserByIdResponse>(`/users/enable/${userId}`, {})
  }

  /**
   *
   * @param userId -
   */
  public resendUserConfirmationEmail(userId: string | number) {
    return this.$httpPost<ST.resendUserConfirmationEmailResponse>(`/users/${userId}/resend_confirmation`, {})
  }
}
