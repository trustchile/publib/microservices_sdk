import * as DTO from './dto'

export type CreateUser = Omit<typeof DTO.CreateUser.prototype, 'role'> & { roles: typeof DTO.CreateUser.prototype.role }
export type UpdateUser = typeof DTO.UpdateUser.prototype
