import { DTO } from './index'

export type CreateSegment = typeof DTO.CreateSegment.prototype
export type UpdateSegment = typeof DTO.UpdateSegment.prototype
