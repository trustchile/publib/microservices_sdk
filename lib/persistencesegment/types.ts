import * as GT from '../common/types'

export interface ServiceResponse<D = any> {
  code: number
  resource: 'Segment' | 'Segment removed'
  data: D
  message: string
}

export interface SegmentData {
  id: number
  index_query: IndexQuery
  created_at: string
  updated_at: string
  uuid: string
  company_id: string
  name: string
}

export interface IndexQuery {
  'apps.app_id'?: string[]
  'apps.person.dni'?: string[]
  'apps.created_at'?: [string, string]
  'apps.tags.systemic_sub_company'?: string
  address?: string
  system_name?: string[]
  os_version?: string[]
  brand?: string[]
  model?: string[]
  created_at?: [string, string]

  [key: string]: string[] | string | undefined
}

export interface PlainFilters {
  address?: string
  application: {
    id: number
  }
  device?: {
    system_name?: string[]
    os_version?: string[]
    model?: string[]
    brand?: string[]
    created_at?: string[]
    last_activity?: string[]
  }
  person?: {
    dni?: string[]
    group?: {
      name?: string
      uuid: string
    }
  }
  tags?: { key: string; value: string }[]
}

export type FilterDevice = Pick<IndexQuery, 'system_name' | 'os_version' | 'model' | 'brand' | 'created_at'> & {
  last_activity?: [string, string]
}

export type createOneResponse = ServiceResponse<
  { name: string; company_id: string; index_query: IndexQuery } & GT.TimestampAble & { uuid: string } & { id: number }
>
export type findOneByIdResponse = ServiceResponse<SegmentData>
export type findAllByCompanyIdResponse = ServiceResponse<SegmentData[]> & GT.Paginated & { total: number }
export type removeOneByIdResponse = ServiceResponse<SegmentData>
