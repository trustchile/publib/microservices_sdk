import { Type } from 'class-transformer'
import {
  IsDefined,
  ArrayMaxSize,
  IsArray,
  IsOptional,
  IsString,
  ArrayMinSize,
  IsObject,
  IsNotEmptyObject,
  ValidateNested,
  IsNumber
} from 'class-validator'

export class PlainFiltersApplication {
  @IsDefined()
  @IsNumber()
  id!: number
}

export class PlainFiltersDevice {
  @IsOptional()
  @IsArray()
  @IsString({ each: true })
  system_name?: string[]

  @IsOptional()
  @IsArray()
  @IsString({ each: true })
  os_version!: string[]

  @IsOptional()
  @IsArray()
  @IsString({ each: true })
  model!: string[]

  @IsOptional()
  @IsArray()
  @IsString({ each: true })
  brand!: string[]

  @IsOptional()
  @IsArray()
  @ArrayMinSize(2)
  @ArrayMaxSize(2)
  @IsString({ each: true })
  created_at!: string[]

  @IsOptional()
  @IsArray()
  @ArrayMinSize(2)
  @ArrayMaxSize(2)
  @IsString({ each: true })
  last_activity!: string[]
}

export class PlainFiltersTags {
  @IsDefined()
  @IsString()
  key!: string

  @IsDefined()
  @IsString()
  value!: string
}

export class PlainFiltersPerson {
  @IsOptional()
  @IsArray()
  @IsString({ each: true })
  dni!: string[]
}

export class PlainFilters {
  @IsDefined()
  @IsObject()
  @IsNotEmptyObject()
  @ValidateNested()
  @Type(() => PlainFiltersApplication)
  application!: PlainFiltersApplication

  @IsOptional()
  @IsObject()
  @IsNotEmptyObject()
  @ValidateNested()
  @Type(() => PlainFiltersDevice)
  device?: PlainFiltersDevice

  @IsOptional()
  @IsString()
  address?: string

  @IsOptional()
  @IsArray()
  @ValidateNested()
  @Type(() => PlainFiltersTags)
  tags?: PlainFiltersTags[]

  @IsOptional()
  @IsObject()
  @IsNotEmptyObject()
  @ValidateNested()
  @Type(() => PlainFiltersPerson)
  person?: PlainFiltersPerson
}

export class CreateSegment {
  @IsString()
  company_id!: string

  @IsString()
  name!: string

  @IsDefined()
  @IsObject()
  @IsNotEmptyObject()
  @Type(() => PlainFilters)
  filters!: PlainFilters
}

export class UpdateSegment {
  @IsOptional()
  @IsString()
  company_id?: string

  @IsOptional()
  @IsString()
  name?: string

  @IsOptional()
  @IsObject()
  @IsNotEmptyObject()
  @Type(() => PlainFilters)
  filters?: PlainFilters
}
