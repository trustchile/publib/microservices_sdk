import { format, parse } from 'date-fns'
import { Singleton, HttpClient } from '../common'
import { ST, DAO } from './index'

@Singleton()
export class Service extends HttpClient {
  /**
   *
   * @param segmentId - Segment ID often as UUID
   */
  public async findOneById(segmentId: string) {
    const response = await this.$httpGet<ST.findOneByIdResponse>(`/api/v1/segment/${segmentId}`)
    return {
      ...response.data,
      filters: this.queryToPlain(response.data.index_query)
    }
  }

  /**
   *
   * @param companyId -
   * @param pagination -
   */
  public async findAllByCompanyId(
    companyId: string | number,
    pagination: { page: number; per_page: number } = { page: 1, per_page: 5 },
    search?: string
  ) {
    return this.$httpGet<ST.findAllByCompanyIdResponse>(`/api/v1/company_id/${companyId}/segment`, {
      ...pagination,
      search
    })
  }

  /**
   *
   * @param segmentId -
   */
  public removeOneById(segmentId: string) {
    return this.$httpDelete(`/api/v1/segment/${segmentId}`)
  }

  /**
   *
   * @param segment -
   */
  public createOne(segment: DAO.CreateSegment) {
    return this.$httpPost<ST.createOneResponse>(`/api/v1/segment`, {
      ...segment,
      index_query: this.plainToQuery(segment.filters)
    })
  }

  /**
   *
   * @param segmentId -
   * @param segment -
   */
  public updateById(segmentId: string, segment: DAO.UpdateSegment) {
    return this.$httpPut(`/api/v1/segment/${segmentId}`, {
      ...segment,
      index_query: segment.filters ? this.plainToQuery(segment.filters) : undefined
    })
  }

  /**
   * Tooks a "Filter" Object and format to an "IndexService" Query
   * @param query -
   * @deprecated -
   */
  public queryToPlain(query: ST.IndexQuery): ST.PlainFilters {
    return {
      address: query.address ?? '',
      application: {
        id: Number(query['apps.app_id']) ?? 1
      },
      device: {
        system_name: query.system_name ?? [],
        os_version: query.os_version ?? [],
        model: query.model ?? [],
        brand: query.brand ?? [],
        created_at: query.created_at ?? [],
        last_activity: query['apps.created_at'] ?? []
      },
      tags: Object.keys(query).reduce((accumulator, queryKey) => {
        if (queryKey.startsWith('apps.tags.')) {
          const tag = query[queryKey] as string[]
          const tagName = queryKey.split('.')[3]
          tag.forEach((tagValue) => {
            accumulator.push({ key: tagName, value: tagValue })
          })
        }
        return accumulator
      }, [] as { key: string; value: string }[]),
      person: {
        dni: query['apps.person.dni'] ?? [],
        group: { uuid: query['apps.tags.systemic_sub_company'] ?? '', name: '' }
      }
    }
  }

  /**
   * Tooks an IndexService Query an parses to a "Filter" Object
   * @param filters -
   * @deprecated -
   */
  public plainToQuery(filters: ST.PlainFilters): ST.IndexQuery {
    const { application, device, address, tags = [] } = filters
    const system_name = device?.system_name
    const os_version = device?.os_version
    const model = device?.model
    const brand = device?.brand
    const created_at = device?.created_at
    const last_activity = device?.last_activity

    return {
      'apps.app_id': String(application.id),

      // apps.tags.TAG_NAME = TAG_VALUE
      ...tags.reduce((accumulator: any, tag: { key: string; value: string }) => {
        const tagAddress = `apps.tags.${tag.key}`
        if (tag.key !== '' && tag.value !== '') {
          if (!Object.keys(accumulator).includes(tagAddress)) {
            accumulator[tagAddress] = []
          }

          accumulator[tagAddress].push(tag.value)
        }
        return accumulator
      }, {}),

      get 'apps.tags.systemic_sub_company'() {
        return filters.person?.group?.uuid ?? undefined
      },

      get 'apps.person.dni'() {
        if (
          filters &&
          filters.person &&
          filters.person.dni &&
          Array.isArray(filters.person.dni) &&
          filters.person.dni.length > 0 &&
          filters.person.dni[0] !== ''
        ) {
          return filters.person.dni
        } else {
          return undefined
        }
      },
      get 'apps.created_at'() {
        return Array.isArray(last_activity) &&
          last_activity.length > 0 &&
          last_activity[0] !== '' &&
          last_activity[1] !== ''
          ? [
              format(parse(last_activity[0], 'yyyy-MM', new Date()), 'yyyy-MM-dd'),
              format(parse(last_activity[1], 'yyyy-MM', new Date()), 'yyyy-MM-dd')
            ]
          : undefined
      },
      get address() {
        return address && address !== '' ? address : undefined
      },
      get system_name() {
        return Array.isArray(system_name) && system_name.length > 0 ? system_name : undefined
      },
      get os_version() {
        return Array.isArray(os_version) && os_version.length > 0 ? os_version : undefined
      },
      get brand() {
        return Array.isArray(brand) && brand.length > 0 ? brand : undefined
      },
      get model() {
        return Array.isArray(model) && model.length > 0 ? model : undefined
      },
      get created_at() {
        return Array.isArray(created_at) && created_at.length > 0 && created_at[0] !== '' && created_at[1] !== ''
          ? [
              format(parse(created_at[0], 'yyyy-MM', new Date()), 'yyyy-MM-dd'),
              format(parse(created_at[1], 'yyyy-MM', new Date()), 'yyyy-MM-dd')
            ]
          : undefined
      }
    }
  }
}
