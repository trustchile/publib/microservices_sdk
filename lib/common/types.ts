export interface Paginated {
  page: number
  per_page: number
}

export interface PaginatedResponse {
  pagination: {
    itemsPerPage: number
    currentPage: number
    totalItems: number
  }
}

export interface Anything<V = any> {
  [key: string]: V
}

export interface IPagination {
  page: number
  per_page: number
}

export interface PaginatedServiceBaseResponse {
  page: number
  per_page: number
  total: number
}

export interface TimestampAble {
  created_at: string
  updated_at: string
}

export interface IdAble {
  _id: { $oid: string }
}

export interface IAmbient {
  uri?: {
    index?: string
    audit?: string
    company?: string
    campaign?: string
    channel?: string
    identification?: string
    notifications?: string
    signal?: string
    work?: string
    persistence?: {
      message?: string
    }
  }
  axiosConfig: any
}

export interface IUris {
  company: string
}

export interface ISearch {
  search<T>(searchData: { [key: string]: string[] }, filterFields: string[], paginationQuery: any): Promise<T>
}

export interface IAuditTransaction {
  method: string
  timestamp: number
  operation: string
  result: string
  type: string
}

export interface IAuditApplication {
  readonly application: string
}

export interface IButton {
  type: string
  text: string
  color: string
  action: string
}

export interface IDialogMessage {
  type: 'dialog'
  notificationDialog: {
    text_body: string
    image_url: string
    isPersistent: boolean
    isCancelable: boolean
    buttons: IButton[]
  }
}

export interface IBannerMessage {
  type: 'banner'
  notificationDialog: {
    text_body: string
    image_url: string
    isPersistent: boolean
    isCancelable: boolean
    buttons: IButton[]
  }
}

export interface IVideoMessage {
  type: 'video'
  notificationVideo: {
    video_url: string
    min_play_time: number
    isPersistent: boolean
    buttons: IButton[]
  }
}

export interface INotificationMessage {
  type: 'notification'
  notificationBody: {
    text_title: string
    text_body: string
    image_url: string
    buttons: IButton[]
  }
}

export interface IIndexSearchData {
  'apps.app_id'?: string
  'apps.update_at'?: string
  address?: string
}

export interface IDefaultSignalServiceResponse<T> {
  status: true
  message: 'Found'
  signal: T[]
  page: number
  per_page: number
  total: number
}

export interface IDefaultSignalData {
  location: [number, number]
  signal: number
  trust_id: string
}
