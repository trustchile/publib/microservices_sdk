import { validateOrReject } from 'class-validator'
import { plainToClass } from 'class-transformer'

export function Validate(dtoClass: any) {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  return function (target: any, propertyKey: string | symbol, parameterIndex: number) {
    const result = plainToClass(dtoClass, target)
    validateOrReject(result)
  }
}
