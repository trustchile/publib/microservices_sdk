import { registerDecorator, ValidationOptions, ValidationArguments } from 'class-validator'

export function IsBundleId(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'isBundleId',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
          const [relatedPropertyName] = args.constraints
          const relatedValue = (args.object as any)[relatedPropertyName]
          return typeof value === 'string' && typeof relatedValue === 'string' && value.split('.').length >= 3
        }
      }
    })
  }
}
