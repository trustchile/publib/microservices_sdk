import { IsInt, IsOptional, Min } from 'class-validator'

export class WantedPagination {
  @IsInt()
  @Min(1)
  wantedPage!: number

  @IsOptional()
  wantedItemsPerPage?: number = 5
}
