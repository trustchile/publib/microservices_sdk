export function allSettled(promises: any): any {
  return Promise.all(
    promises.map((p: any) => {
      return Promise.resolve(p).then(
        (val) => ({ state: 'fulfilled', value: val.data }),
        (err) => ({
          state: 'rejected',
          value: {
            status: err.status,
            err: err,
            message: err.message,
            config: JSON.parse(err.config.data)
          }
        })
      )
    })
  ).then((response: any) => {
    return response.reduce(
      (accumulator: any, currentValue: any) => {
        accumulator[currentValue.state].push(currentValue.value)
        return accumulator
      },
      {
        fulfilled: [],
        rejected: []
      }
    )
  })
}
