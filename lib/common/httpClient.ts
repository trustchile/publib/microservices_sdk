import { GaxiosResponse, request, GaxiosOptions } from 'gaxios'
import { Anything } from '../common/types'

export { GaxiosError as IError } from 'gaxios'

/**
 * Conveniently wraps Gaxios 'request' function into Axios-like methods
 * @param baseURL -
 */
export class HttpClient {
  public baseURL: string | null = null
  public accessToken: string | null = null
  public debug: boolean = false
  public secure: boolean = true

  /**
   * Http Internal method factory
   * @param method -
   */
  private make<Response, Request>(method: NonNullable<GaxiosOptions['method']>) {
    return async (url: string, params: Anything, data: Request, headers: Anything) => {
      if (this.debug) {
        console.log('[TRUST LIB DEBUG ----START----]')
        console.log(`${method}:${this.baseURL}${url}`)
        console.log(`[REQUEST META] => `, { headers, params })
        console.log(`[REQUEST DATA] => `, { data })
        console.log('STARTING REQUEST...')
      }

      const requestStartedAt = new Date().getTime()

      const response = await request<Response>({
        baseURL: String(this.baseURL),
        method,
        data,
        url,
        params,
        headers: {
          ...headers,
          Authorization: `Bearer ${this.accessToken}`
        }
      })

      if (this.debug) {
        console.log(`END REQUEST, TOOKED ${new Date().getTime() - requestStartedAt}ms`)
        console.log(`[RESPONSE META] => `, { status: response.status })
        console.log(`[RESPONSE DATA]`, { data: response.data })
        console.log('[TRUST LIB DEBUG ----END----]')
      }

      return (response?.data ?? null) as GaxiosResponse<Response>['data']
    }
  }

  /**
   *
   * @param url -
   * @param params -
   * @param headers -
   */
  protected $httpGet<Response = any>(url: string, params = {}, headers = {}) {
    return this.make<Response, any>('GET')(url, params, undefined, headers)
  }

  /**
   *
   * @param url -
   * @param data -
   * @param headers -
   */
  protected $httpPost<Response = any, Request = any>(url: string, data: any, params = {}, headers = {}) {
    return this.make<Response, Request>('POST')(url, params, data, headers)
  }

  /**
   *
   * @param url -
   * @param data -
   * @param headers -
   */
  protected $httpPut<Response = any, Request = any>(url: string, data: any, params = {}, headers = {}) {
    return this.make<Response, Request>('PUT')(url, params, data, headers)
  }

  /**
   *
   * @param url -
   * @param data -
   * @param headers -
   */
  protected $httpPatch<Response = any, Request = any>(url: string, data: any, params = {}, headers = {}) {
    return this.make<Response, Request>('PATCH')(url, params, data, headers)
  }

  /**
   *
   * @param url -
   * @param params -
   * @param headers -
   */
  protected $httpDelete<Response = any>(url: string, params = {}, headers = {}) {
    return this.make<Response, any>('DELETE')(url, params, undefined, headers)
  }

  /**
   *
   */
  public httpClient() {
    return {
      $get: this.$httpGet,
      $post: this.$httpPost,
      $put: this.$httpPut,
      $patch: this.$httpPatch,
      $delete: this.$httpDelete
    }
  }
}
