import { Singleton, HttpClient, allSettled } from '../common'

/**
 *
 */
@Singleton()
export class Service extends HttpClient {
  /**
   *
   * @param target -
   * @param payload -
   */
  public async sendMessageToMany(target: { devices: string[]; appName: string; messageBodyKey: string }, payload: any) {
    return await allSettled(
      target.devices.map((trustId) => {
        return this.$httpPost('/device/sendmessage', {
          trust_id: trustId,
          application_name: target.appName,
          priority: 'high',
          apns: {
            payload: {
              aps: {
                'mutable-content': 1,
                sound: 'default',
                category: 'url-call',
                alert: {
                  title: payload.data.text_title || ' ',
                  body: payload.data.text_body || ' '
                },
                badge: 1
              },
              'url-scheme': 'https://google.com',
              data: {
                type: payload.type,
                [target.messageBodyKey]: JSON.stringify(payload.data)
              }
            }
          },
          data: {
            type: payload.type,
            [target.messageBodyKey]: JSON.stringify(payload.data)
          }
        })
      })
    )
  }

  /**
   *
   * @param trustId -
   * @param appName -
   */
  public async sendLockToTrustId(trustId: string, appName: string) {
    const { data } = await this.$httpPost('/device/sendmessage', {
      trust_id: trustId,
      application_name: appName,
      priority: 'high',
      data: {
        type: 'lock',
        state_lock: 'si'
      }
    })
    return data
  }

  /**
   *
   * @param trustId -
   * @param appName -
   */
  public async sendUnlockToTrustId(trustId: string, appName: string) {
    const { data } = await this.$httpPost('/device/sendmessage', {
      trust_id: trustId,
      application_name: appName,
      priority: 'high',
      data: {
        type: 'lock',
        state_lock: 'no'
      }
    })
    return data
  }

  /**
   *
   * @param trustId -
   * @param appName -
   */
  public async sendSignalToTrustId(trustId: string, appName: string) {
    const { data } = await this.$httpPost('/device/sendmessage', {
      trust_id: trustId,
      application_name: appName,
      priority: 'high',
      data: {
        firebase: 'true',
        listener: 'false',
        schedule: 'false',
        recurrency: '24'
      }
    })
    return data
  }

  /**
   *
   * @param trustId -
   * @param appName -
   */
  public async sendLocationToTrustId(trustId: string, appName: string) {
    const { data } = await this.$httpPost('/device/sendmessage', {
      trust_id: trustId,
      application_name: appName,
      priority: 'high',
      data: {
        firebase: 'true',
        listener: 'false',
        schedule: 'false',
        recurrency: '24'
      }
    })
    return data
  }
}
