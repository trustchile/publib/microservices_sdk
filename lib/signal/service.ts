import { HttpClient, Singleton } from '../common'

/**
 *
 */
@Singleton()
export class Service extends HttpClient {
  /**
   *
   * @param trustId -
   * @deprecated -
   */
  public async findSignalAndLocationByTrustId(trustId: string) {
    const response = await this.$httpGet<{ signal: any[] }>(`/api/v1/device/${trustId}/signal/`)
    return response?.signal?.[0] ?? null
  }

  /**
   *
   * @param bundleId -
   * @param filterFields -
   * @param paginationQuery -
   * @deprecated -
   */
  public async query(/*bundleId: string, filterFields: string[] = [], paginationQuery: any = { page: 1, per_page: 1 }*/) {
    //const { data } = await this.$httpGet('/api/v1/signal/query', {
    //  params: {
    //    ...paginationQuery,
    //    fields: filterFields.join(','),
    //    bundle_id: bundleId
    //  },
    //  paramsSerializer: (params) => {
    //    return qs.stringify(params, {
    //      encode: false,
    //      arrayFormat: 'brackets'
    //    })
    //  }
    //})
    //return data
  }

  /**
   *
   * @param bundle_id -
   * @deprecated -
   */
  public async findByBundleId(/*bundle_id: string[]*/) {
    //const { data } = await this.$httpGet('/api/v1/signal/query', {
    //  params: {
    //    page: 1,
    //    per_page: 10000,
    //    bundle_id,
    //    fields: 'location,signal,trust_id'
    //  },
    //  paramsSerializer: (params) => {
    //    return qs.stringify(params, {
    //      encode: false,
    //      arrayFormat: 'brackets'
    //    })
    //  }
    //})
    //return data
  }

  /**
   *
   * @param bundle_id -
   * @deprecated -
   */
  public async getByBundleId(/*bundle_id: string[]*/) {
    //const { data } = await this.$httpGet('/api/v1/signal/query', {
    //  params: {
    //    page: 1,
    //    per_page: 1,
    //    bundle_id,
    //    fields: ''
    //  },
    //  paramsSerializer: (params) => {
    //    return qs.stringify(params, {
    //      encode: false,
    //      arrayFormat: 'brackets'
    //    })
    //  }
    //})
    //const totalRegistros = data.total
    //const pages = totalRegistros / 100
    //if (totalRegistros <= 100) {
    //  const { data } = await this.$httpGet('/api/v1/signal/query', {
    //    params: {
    //      page: 1,
    //      per_page: 100,
    //      bundle_id,
    //      fields: 'location,signal,trust_id,bundle_id'
    //    },
    //    paramsSerializer: (params) => {
    //      return qs.stringify(params, {
    //        encode: false,
    //        arrayFormat: 'brackets'
    //      })
    //    }
    //  })
    //  return [data.signal]
    //} else {
    //  const promises = []
    //  for (let i = 1; i <= pages; i++) {
    //    promises.push(
    //      SignalService.$HttpClient()
    //        .get('/api/v1/signal/query', {
    //          params: {
    //            page: i,
    //            per_page: 100,
    //            bundle_id,
    //            fields: 'location,signal,trust_id,bundle_id'
    //          },
    //          paramsSerializer: (params) => {
    //            return qs.stringify(params, {
    //              encode: false,
    //              arrayFormat: 'brackets'
    //            })
    //          }
    //        })
    //        .then((response) => response.data.signal)
    //    )
    //  }
    //  //@ts-ignore
    //  return await Promise.all(promises)
    //}
  }
}
