export * as ST from './types'
export * from './service'

import { Service } from './service'

export const instance = new Service()
