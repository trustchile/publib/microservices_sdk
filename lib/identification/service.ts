import { Singleton, HttpClient } from '../common'
import { ST } from './index'

/**
 *
 */
@Singleton()
export class Service extends HttpClient {
  private readonly defaultSearchFields: string[] = [
    'trust_id',
    'created_at',
    'characteristics.imei',
    'characteristics.system_name',
    'characteristics.system_version',
    'characteristics.model',
    'characteristics.brand'
  ]

  /**
   *
   * @param trustId -
   * @param fieldParams -
   */
  public async findAllDevices<T = ST.DefaultSearchFields>(
    trustId: string[],
    fieldParams: string[] = this.defaultSearchFields
  ) {
    const response = await this.$httpPost<ST.findAllDevicesResponse<T[]>>(
      `/api/v1/device/search`,
      {
        search: trustId
      },
      {
        fields: fieldParams.join(',')
      }
    )
    return response?.result ?? []
  }

  /**
   *
   * @param trustId -
   */
  public async findDeviceByTrustId(trustId: string) {
    const response = await this.$httpGet<ST.findDeviceByTrustIdResponse>(`/api/v1/device/${trustId}`)
    return response?.device.device ?? null
  }

  /**
   *
   * @param trustId -
   */
  public async findIdentityByTrustId(trustId: string) {
    const response = await this.$httpGet<ST.findIdentityByTrustIdResponse>(`/api/v1/device/${trustId}/identity`)
    return (response?.identity ?? null) as ST.Identity | null
  }

  /**
   *
   * @param dni -
   */
  public async findLastDeviceByDni(dni: string) {
    const response = await this.$httpGet<ST.findLastDeviceByDniResponse>(`/api/v1/identity/${dni}/lastdevice`)
    return response?.device ?? null
  }

  /**
   *
   * @param dni -
   * @param fields -
   */
  public async findDevicesByDni(
    dni: string,
    fields: string[] = [
      'characteristics.imei',
      'characteristics.serial',
      'characteristics.brand',
      'characteristics.model',
      'trust_id'
    ]
  ) {
    const response = await this.$httpGet<ST.findDevicesByDniResponse>(`/api/v1/identity/${dni}/device`, {
      fields: fields.join(', ')
    })
    return response?.devices ?? []
  }

  /**
   *
   * @param dni -
   */
  public async findIdentityByDni(dni: string) {
    const response = await this.$httpGet<ST.findIdentityByDniResponse>(`/api/v1/identity/${dni}`)
    return response?.identity ?? null
  }

  /**
   *
   * @param dni -
   */
  public async findAllIdentities(dni: string[]) {
    const response = await this.$httpPost<ST.findAllIdentitiesResponse>(`/api/v1/identity/dni`, {
      data: {
        dnis: dni
      }
    })
    return response?.user ?? []
  }
}
