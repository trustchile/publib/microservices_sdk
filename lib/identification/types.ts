export interface DefaultSearchFields {
  id: number
  trust_id: string
  created_at: string
  imei: string
  system_name: string
  system_version: string
  model: string
  brand: string
}

export interface MinimumResponse {
  status: boolean
}

export interface Device {
  id: number
  trust_id: string
  characteristics: DeviceCharacteristics
  created_at: string
  updated_at: string
  status: boolean
}

export interface DeviceCharacteristics {
  id: string
  nfc: string
  UUID: string
  host: string
  imei: string
  root: string
  board: string
  brand: string
  model: string
  device: string
  serial: string
  battery: string
  cameras: {
    type: 'BACK' | 'FRONT'
    mega_pixels: string
    focal_length: string
    max_exposure_comp: string
    min_exposure_comp: string
    vertical_view_angle: string
    horizontal_view_angle: string
  }[]
  display: string
  product: string
  cpu_part: string
  hardware: string
  mem_total: string
  wlan0_mac: string
  sensorData: {
    name: string
    vendor: string
  }[]
  swap_total: string
  wifi_state: boolean
  cpu_variant: string
  fingerprint: string
  red_g_state: boolean
  sensor_size: string
  system_name: string
  cameras_size: string
  cpu_revision: string
  manufacturer: string
  bluetooth_mac: string
  system_version: string
  bluetooth_state: boolean
  cpu_implementer: string
  battery_capacity: string
  cpu_architecture: string
  software_version: string
  android_device_id: string
  battery_technology: string
  processor_bogomips: string
  processor_features: string
  processor_quantity: string
  google_service_framework_gsf: string
}

export interface Identity {
  id: number
  dni: string
  name: string
  lastname: string
  email: string
  phone: string
  created_at: string
  updated_at: string
  status: boolean
  uuid: string
}

export interface DefaultIdentificationResponse {
  id: number
  trust_id: string
  imei: string
  serial: string
  brand: string
  model: string
}

export type findAllIdentitiesResponse = MinimumResponse & { user: Identity[] }
export type findIdentityByDniResponse = MinimumResponse & { identity: Identity }
export type findAllDevicesResponse<T> = MinimumResponse & { result: T }
export type findDeviceByTrustIdResponse = MinimumResponse & { device: { device: Device } }
export type findIdentityByTrustIdResponse = MinimumResponse & { identity: Identity }
export type findLastDeviceByDniResponse = MinimumResponse & { dni: string; device: Device }
export type findDevicesByDniResponse = MinimumResponse & { devices: Device[]; dni: string }
