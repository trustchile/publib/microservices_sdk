import * as PACKAGE from './package.json'
import { Service as AuditService, instance as AuditInstance, ST as AuditTypes } from './lib/audit'
import { Service as AteneaService, instance as AteneaInstance, ST as AteneaTypes } from './lib/atenea'
import { Service as CompanyService, instance as CompanyInstance, ST as CompanyTypes } from './lib/company'
import { Service as CampaignsService, instance as CampaignsInstance, ST as CampaignsTypes } from './lib/campaigns'
import {
  Service as IdentificationService,
  instance as IdentificationInstance,
  ST as IdentificationTypes
} from './lib/identification'
import { Service as NotificationsService, instance as NotificationsInstance } from './lib/notifications'
import { Service as IndexService, instance as IndexInstance, ST as IndexTypes } from './lib/indexs'
import { Service as ChannelService, instance as ChannelInstance, ST as ChannelTypes } from './lib/channel'
import {
  Service as MessagePersistenceService,
  instance as MessagePersistenceInstance,
  ST as MessagePersistenceTypes
} from './lib/persistencemessage'
import {
  Service as SegmentPersistenceService,
  instance as SegmentPersistenceInstance,
  ST as SegmentPersistenceTypes,
  DTO as SegmentPersistenceDTO
} from './lib/persistencesegment'
import { Service as WorkService, instance as WorkInstance, ST as WorkTypes } from './lib/work'
import { Service as SignalService, instance as SignalInstance } from './lib/signal'

export const version = PACKAGE.version

export function setupSDKFromEnv(environment: any, prefix: string = 'SERVICE_URI') {
  new AuditService()
  new AteneaService()
  new CompanyService()
  new CampaignsService()
  new IdentificationService()
  new NotificationsService()
  new IndexService()
  new ChannelService()
  new MessagePersistenceService()
  new SegmentPersistenceService()
  new WorkService()
  new SignalService()

  AuditInstance.baseURL = environment[`${prefix}_AUDIT`]
  AteneaInstance.baseURL = environment[`${prefix}_AUTH`]
  CompanyInstance.baseURL = environment[`${prefix}_COMPANY`]
  CampaignsInstance.baseURL = environment[`${prefix}_CAMPAIGNS`]
  IdentificationInstance.baseURL = environment[`${prefix}_IDENTIFICATION`]
  NotificationsInstance.baseURL = environment[`${prefix}_NOTIFICATIONS`]
  IndexInstance.baseURL = environment[`${prefix}_INDEX`]
  ChannelInstance.baseURL = environment[`${prefix}_CHANNELS`]
  MessagePersistenceInstance.baseURL = environment[`${prefix}_MESSAGE_PERSISTENCE`]
  SegmentPersistenceInstance.baseURL = environment[`${prefix}_SEGMENT_PERSISTENCE`]
  WorkInstance.baseURL = environment[`${prefix}_WORK`]
  SignalInstance.baseURL = environment[`${prefix}_SIGNAL`]
}

export function setAccessToken(token: string) {
  AuditInstance.accessToken = token
  AteneaInstance.accessToken = token
  CompanyInstance.accessToken = token
  CampaignsInstance.accessToken = token
  IdentificationInstance.accessToken = token
  NotificationsInstance.accessToken = token
  IndexInstance.accessToken = token
  ChannelInstance.accessToken = token
  MessagePersistenceInstance.accessToken = token
  SegmentPersistenceInstance.accessToken = token
  WorkInstance.accessToken = token
  SignalInstance.accessToken = token
}

export {
  AuditService,
  AteneaService,
  CompanyService,
  CampaignsService,
  IdentificationService,
  NotificationsService,
  IndexService,
  ChannelService,
  MessagePersistenceService,
  SegmentPersistenceService,
  WorkService,
  SignalService,
  AuditInstance,
  AteneaInstance,
  CompanyInstance,
  CampaignsInstance,
  IdentificationInstance,
  NotificationsInstance,
  IndexInstance,
  ChannelInstance,
  MessagePersistenceInstance,
  SegmentPersistenceInstance,
  WorkInstance,
  SignalInstance,
  AuditTypes,
  AteneaTypes,
  CompanyTypes,
  CampaignsTypes,
  IdentificationTypes,
  IndexTypes,
  ChannelTypes,
  MessagePersistenceTypes,
  SegmentPersistenceTypes,
  WorkTypes,
  SegmentPersistenceDTO
}
