import { request } from 'gaxios'

export async function generateAccessToken() {
  const { data } = await request<any>({
    method: 'POST',
    url: `${process.env.SERVICE_URI_ATENEA}/oauth/token/`,
    data: {
      client_secret: process.env.APP_AUTH_CLIENT_SECRET,
      client_id: process.env.APP_AUTH_CLIENT_ID,
      grant_type: 'client_credentials'
    }
  })
  return (data?.access_token ?? '') as string
}
