import dotenv from 'dotenv'
dotenv.config()

import { generateAccessToken } from '../test/helpers/token'
import { MessagePersistenceService, MessagePersistenceInstance } from '../index'

const SERVICE_URI_MESSAGE_PERSISTENCE = String(process.env.SERVICE_URI_MESSAGE_PERSISTENCE)
const $MessagePersistence = new MessagePersistenceService()

describe('Message persistence service behavior', () => {
  before(async () => {
    $MessagePersistence.baseURL = SERVICE_URI_MESSAGE_PERSISTENCE
    $MessagePersistence.accessToken = await generateAccessToken()
  })

  it('should find at least one message', async () => {
    const response = await $MessagePersistence.findOneById('c23004c6-bc6f-43a8-a046-c5d712a70e7a')
    console.log({ serviceResponse: response })
  })
})
